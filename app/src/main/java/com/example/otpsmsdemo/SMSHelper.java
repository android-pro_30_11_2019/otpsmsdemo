package com.example.otpsmsdemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.SmsManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class SMSHelper {
    public static final int SEND_SMS_REQUEST_CODE = 168;
    public static final int READ_PHONE_STATE_REQUEST_CODE = 169;

    public static void sendSMS(Activity activity, String phoneNumber, String smsBody) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber,null,smsBody,null,null);
        }catch (Exception e){
            e.printStackTrace();//Display errros;
            //For Android 8.0
            if (e.toString().contains(Manifest.permission.READ_PHONE_STATE) && ContextCompat
                    .checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE)!=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission
                        .READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST_CODE);
            }
        }
    }
}
