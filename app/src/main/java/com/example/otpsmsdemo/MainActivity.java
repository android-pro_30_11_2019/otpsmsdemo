package com.example.otpsmsdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.otpsmsdemo.databinding.ActivityMainBinding;

import java.net.StandardSocketOptions;

import static com.example.otpsmsdemo.SMSHelper.READ_PHONE_STATE_REQUEST_CODE;
import static com.example.otpsmsdemo.SMSHelper.SEND_SMS_REQUEST_CODE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityMainBinding binding;
    IntentFilter intentFilter;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            binding.tvCode.setText("Your OTP: "+intent.getExtras().getString("sms"));
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver,intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void finish() {
        super.finish();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnSend.setOnClickListener(this);

        //Handle Request runtime Permission;
        int checked = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if(checked != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS},
                    SEND_SMS_REQUEST_CODE
            );
        }
        //Initilize Itent Fileter
        intentFilter = new IntentFilter();
        intentFilter.addAction(SMSReceiver.MY_SMS_ACTION_SEND);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case SEND_SMS_REQUEST_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Send SMS Permission Granted", Toast.LENGTH_SHORT).show();
                }
                break;
            case READ_PHONE_STATE_REQUEST_CODE:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Send SMS Permission Granted", Toast.LENGTH_SHORT).show();
                    String phoneNumber = binding.etPhoneNumber.getText().toString();
                    String smsBody = binding.etOTP.getText().toString();
                    SMSHelper.sendSMS(this,phoneNumber,smsBody);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSend:
                //Logic Here.
                Toast.makeText(this, "Testing", Toast.LENGTH_SHORT).show();
                String phoneNumber = binding.etPhoneNumber.getText().toString();
                String smsBody = binding.etOTP.getText().toString();
                SMSHelper.sendSMS(this,phoneNumber,smsBody);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }
}
